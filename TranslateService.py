from googletrans import Translator
import googletrans


def get_all_lang():
    try:
        return googletrans.LANGCODES
    except:
        return None


def check_lang(abr):
    try:
        checker = googletrans.LANGUAGES.get(abr)
        return checker
    except:
        return None


def translate(word, lang):
    try:
        translator = Translator()
        return translator.translate(word, lang).text
    except:
        return "not tranlated"
