import TranslateService
import json
from TranslationTemplate import TranslationTemplate
from TranslationTemplate import TranslationTemplateEncoder
import math
import os


# clear console
def clean_screen():
    clear = lambda: os.system('cls')
    clear()


def return_all_languages():
    allLang = TranslateService.get_all_lang()
    if allLang is None:
        print("Something wrong in TranslateService with googletrans module")
        exit()

    print("________________________________________________________________________")
    print("All languages " + str(len(allLang.keys())))

    # sort the keys alphabetically - GPD
    keys = sorted(allLang.keys())

    # calculate the number of rows and columns - GPD
    num_cols = 8
    num_rows = math.ceil(len(keys) / num_cols)

    # create a list to hold the columns - GPD
    cols = [[] for _ in range(num_cols)]

    # fill in the columns with the keys and values - GPD
    for i, key in enumerate(keys):
        cols[i % num_cols].append(f"{key}: {allLang[key]}")

    # print the columns in a table - GPD
    for i in range(num_rows):
        for j in range(num_cols):
            index = i * num_cols + j  # corrected index calculation
            if index < len(keys):
                print("{:20s}".format(cols[j][i]), end="")
        print()


def check_and_return_language():
    while True:
        inputLang = input("Please enter language abbreviation: ").lower()
        check = TranslateService.check_lang(inputLang)

        if check is not None:
            newName = check
            break
        else:
            print("Language doesn't exist. Please try again.")
    return newName


def return_Translate_Template(inputLang):
    template = TranslationTemplate()
    # change main template
    template.Abbreviation = inputLang
    template.Name = inputLang
    return template


def translate_words_in_template(template):
    print("Translate begin")
    # translate all words in template
    for attr in dir(template.JsonData):
        if not callable(getattr(template.JsonData, attr)) and not attr.startswith("__"):
            # Translate the attribute value
            new_value = TranslateService.translate(getattr(template.JsonData, attr), template.Abbreviation)
            # Set the translated value to the attribute
            setattr(template.JsonData, attr, new_value)

    print("Translate end")
    return template


def create_file(template):
    print("________________________________________________________________________")
    json_string = json.dumps(template, cls=TranslationTemplateEncoder, ensure_ascii=False)

    # download to download folder
    fileName = template.Abbreviation + '.json'
    file_path = os.path.join(os.path.expanduser("~"), "Downloads", fileName)

    with open(file_path, "w", encoding="utf-8") as f:
        f.write(json_string)

    print("File create successfully in Download folder")

def main():
    clean_screen()
    return_all_languages()
    print("________________________________________________________________________")
    inputLang = check_and_return_language()

    # Create an instance of the class
    template = return_Translate_Template(inputLang)

    # # translate all words in template
    template = translate_words_in_template(template)

    # Convert the object to a JSON string using the custom encoder
    # download file to download folder
    create_file(template)

main()
