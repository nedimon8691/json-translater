import json


class TranslationTemplate:
    def __init__(self):
        self.Abbreviation = "en"
        self.Name = "English"
        self.JsonData = JData()



class JData:
    def __init__(self):
        self.name = "Name"
        self.save =  "Save"
        self.add =  "Add"
        # self.unblock =  "Unblock"
        # self.block =  "Block"
        # self.dashboard =  "Dashboard"
        # self.description =  "Desc"
        # self.assetType =  "Asset Type"
        # self.assetCategory =  "Asset Category"
        # self.confirmMsg =  "Are you sure you want to confirm?"
        # self.declineMsg =  "Are you sure you want to decline?"
        # self.approveMsg =  "Are you sure you want to approve?"
        # self.type_your_comment =  "Type your comment"
        # self.comment =  "Comment"
        # self.approve =  "Approve"
        # self.decline =  "Decline"
        # self.confirm =  "Confirm"
        # self.view =  "View"
        # self.deleted =  "Deleted"
        # self.doughnut =  "Doughnut"
        # self.line =  "Line"
        # self.xyChart =  "X Y (Scatter)"
        # self.scatter =  "Scatter"
        # self.bubble =  "Bubble"
        # self.areaChart =  "Area"
        # self.area =  "Area"
        # self.stackedArea =  "Stacked"
        # self.normalizedArea =  "100% Stacked"
        # self.histogramChart =  "Histogram"
        # self.combinationChart =  "Combination"
        # self.columnLineCombo =  "Column & Line"
        # self.AreaColumnCombo =  "Area & Column"
        # self.pivotChartTitle =  "Pivot Chart"

# Custom JSON encoder for TranslationTemplate
class TranslationTemplateEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, TranslationTemplate):
            return {
                'Abbreviation': o.Abbreviation,
                'Name': o.Name,
                'JsonData': o.JsonData.__dict__
            }
        return super().default(o)